function giveFeedback(feedback) {
    if (feedback === 'baik') {
      alert("tirimakasih");
    } else if (feedback === 'buruk') {
      alert("maaf atas ketidaknyamannya");
    }
}

// JavaScript code for loop
console.log('Looping menggunakan for:');
for (let i = 1; i <= 5; i++) {
    console.log(i);
}

console.log('Looping menggunakan while:');
let j = 1;
while (j <= 5) {
    console.log(j);
    j++;
}
